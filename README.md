Functionality of the App:

This app implements the Camera. To use the camera, please follow the below instructions.

1. Proceed to the Home/Dashboard page by clicking the 'Login' button in the 'Login Page'.
2. Click 'Upload Selfie'.
3. Click 'Take Picture'.
4. Take the picture by clicking 'Picture'.
